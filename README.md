---
title: README
date: 2022-02-05T21:28:52+00:00
author: Xyne
---

# About

In late December 2021 the [Arch Linux wiki](https://wiki.archlinux.org/)'s default appearance changed. While there is an option to switch to the old skin, it requires users to be logged in. This is a Greasemonkey script that automatically appends a query parameter to use the old skin even when the user is not logged in.

At the time of writing, there is an ongoing effort to improve the Wiki's appearance and make it more customizable. See the following threads on the Arch Linux forum for more information:

* [WTF happened to the Arch Wiki appearance?](https://bbs.archlinux.org/viewtopic.php?pid=2010840)
* [New Arch Wiki appearance](https://bbs.archlinux.org/viewtopic.php?id=272800)



# Installation

Copy `arch_wiki_old_skin.js` to your browser's Greasemonkey directory.

Note that once changes are made to the Arch wiki, this script will become obsolete and should be removed.
